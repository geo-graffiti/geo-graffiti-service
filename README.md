# GeoGraffiti backend service for the Junction 2023 Hackathon

database schema: https://dbdiagram.io/d/654f3ce77d8bbd6465f87d42

## How to build and run Docker image
1. `./gradlew build`
2. `docker build -t geograffiti-backend .`
3. `docker run -p 8080:8080 geo-graffiti-service`