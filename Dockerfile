FROM openjdk:17-alpine
EXPOSE 8080
ADD /build/libs/geo-graffiti-service-0.0.1-SNAPSHOT.jar geo-graffiti-service.jar
ENTRYPOINT ["java", "-jar", "geo-graffiti-service.jar"]
