package com.junction.geograffitiservice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class GeoGraffitiServiceApplication

fun main(args: Array<String>) {
    runApplication<GeoGraffitiServiceApplication>(*args)
}
