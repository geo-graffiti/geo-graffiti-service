package com.junction.geograffitiservice.repository

import com.junction.geograffitiservice.dao.Figure
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface FigureRepository: JpaRepository<Figure, Int> {
}