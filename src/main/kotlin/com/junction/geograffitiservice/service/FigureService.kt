package com.junction.geograffitiservice.service

import com.junction.geograffitiservice.dao.VertexArray
import com.junction.geograffitiservice.dto.FigureDto
import com.junction.geograffitiservice.repository.FigureRepository
import org.springframework.stereotype.Service

@Service
class FigureService(
    private val figureRepository: FigureRepository
) {

    fun getRecommendedFigures(city: String): List<FigureDto> {
        val allFigures = getAllFigures()
        //TODO: filter out all completed figures + check that tis user has no in-progress figures
        return allFigures.filter { it.city == city }
    }

    private fun getAllFigures(): List<FigureDto> {
        val figures = figureRepository.findAll()
        return figures.map {
            FigureDto(
                id = it.id,
                name = it.name,
                description = it.description,
                city = it.city,
                numberOfVertices = it.numberOfVertices,
                perimeter = it.perimeter,
                score = it.score,
                vertices = VertexArray(it.vertices).vertices
            )
        }.toList()
    }
}