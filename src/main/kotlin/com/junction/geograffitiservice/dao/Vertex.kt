package com.junction.geograffitiservice.dao

import com.fasterxml.jackson.databind.ObjectMapper
import java.io.Serializable

data class Vertex(
    val latitude: Double = 0.0,
    val longitude: Double = 0.0,
    val attraction: String = ""
) : Serializable

data class VertexArray(
    var vertices: List<Vertex> = emptyList()
) : Serializable {

    private val objectMapper: ObjectMapper = ObjectMapper()

    constructor(jsonString: String) : this() {
        this.vertices = objectMapper.readValue(jsonString, VertexArray::class.java).vertices
    }
}