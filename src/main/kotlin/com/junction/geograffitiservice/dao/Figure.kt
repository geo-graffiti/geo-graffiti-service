package com.junction.geograffitiservice.dao

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id


@Entity
data class Figure(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null,

    @Column(nullable = false)
    val name: String = "",

    @Column(nullable = false)
    val description: String = "",

    @Column(nullable = false)
    val city: String = "",

    @Column(nullable = false)
    val numberOfVertices: Int = 0,

    @Column(nullable = false)
    val perimeter: Double = 0.0,

    @Column(nullable = false)
    val score: Double = 0.0,

    @Column(nullable = false)
    val vertices: String = ""
)