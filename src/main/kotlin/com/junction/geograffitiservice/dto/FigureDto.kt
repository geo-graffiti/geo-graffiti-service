package com.junction.geograffitiservice.dto

import com.junction.geograffitiservice.dao.Vertex
import java.io.Serializable

data class FigureDto(
    val id: Long? = null,
    val name: String = "",
    val description: String = "",
    val city: String = "",
    val numberOfVertices: Int = 0,
    val perimeter: Double = 0.0,
    val score: Double = 0.0,
    val vertices: List<Vertex> = emptyList()
): Serializable