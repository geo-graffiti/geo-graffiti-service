package com.junction.geograffitiservice.dto

data class Point(
    val latitude: Double,
    val longitude: Double,
)