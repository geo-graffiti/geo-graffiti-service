package com.junction.geograffitiservice.controller

import com.junction.geograffitiservice.dao.Vertex
import com.junction.geograffitiservice.dto.FigureDto
import com.junction.geograffitiservice.dto.Point
import com.junction.geograffitiservice.service.FigureService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("user/{userId}/figures")
class FiguresController(
    private val figureService: FigureService
) {

    @GetMapping("/recommend")
    fun getRecommendedFigures(
        @PathVariable userId: Int,
        @RequestParam("city", required = true) city: String,
    ): ResponseEntity<List<FigureDto>> {
        //TODO: take into account user's health condition and activity level
        return ResponseEntity.ok(figureService.getRecommendedFigures(city))
    }

    @PostMapping("/start")
    fun startNewFigure(
        @PathVariable userId: Int,
        @RequestParam("figureId", required = true) figureId: Int
    ) {

    }

    @GetMapping("/current")
    fun getCurrentFigure(
        @PathVariable userId: Int
    ) {

    }

    @PostMapping("/current/complete-vertex")
    fun completeVertexOfCurrentFigure(
        @PathVariable userId: Int,
        @RequestBody vertex: Vertex
    ) {

    }

    @PostMapping("/current/closest-vertex")
    fun getClosestVertex(
        @PathVariable userId: Int,
        @RequestParam currentPoint: Point
    ) {

    }
}