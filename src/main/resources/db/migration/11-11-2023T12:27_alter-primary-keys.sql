create sequence figure_id_seq;
alter table figure alter column id set default nextval('figure_id_seq'::regclass);

create sequence user_id_seq;
alter table "user" alter column id set default nextval('user_id_seq'::regclass);