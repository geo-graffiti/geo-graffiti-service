CREATE TYPE "figure_status" AS ENUM (
  'in_progress',
  'completed',
  'failed_to_complete'
);

CREATE TABLE "user"
(
    "id"   integer PRIMARY KEY,
    "name" varchar NOT NULL
);

CREATE TABLE "figure"
(
    "id"        integer PRIMARY KEY,
    "name"      varchar NOT NULL,
    "description" varchar NOT NULL,
    "city"      varchar NOT NULL,
    "vertices"  json NOT NULL,
    "number_of_vertices" integer NOT NULL,
    "score"     float NOT NULL,
    "perimeter" float NOT NULL
);

CREATE TABLE "user_to_figure"
(
    "user"               integer,
    "figure"             integer,
    "status"             figure_status NOT NULL,
    "completed_vertices" json NOT NULL
);

ALTER TABLE "user_to_figure"
    ADD FOREIGN KEY ("user") REFERENCES "user" ("id");

ALTER TABLE "user_to_figure"
    ADD FOREIGN KEY ("figure") REFERENCES "figure" ("id");